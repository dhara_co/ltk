/**
 * Include your custom JavaScript here.
 *
 * We also offer some hooks so you can plug your own logic. For instance, if you want to be notified when the variant
 * changes on product page, you can attach a listener to the document:
 *
 * document.addEventListener('variant:changed', function(event) {
 *   var variant = event.detail.variant; // Gives you access to the whole variant details
 * });
 *
 * You can also add a listener whenever a product is added to the cart:
 *
 * document.addEventListener('product:added', function(event) {
 *   var variant = event.detail.variant; // Get the variant that was added
 *   var quantity = event.detail.quantity; // Get the quantity that was added
 * });
 *
 * If you are an app developer and requires the theme to re-render the mini-cart, you can trigger your own event. If
 * you are adding a product, you need to trigger the "product:added" event, and make sure that you pass the quantity
 * that was added so the theme can properly update the quantity:
 *
 * document.documentElement.dispatchEvent(new CustomEvent('product:added', {
 *   bubbles: true,
 *   detail: {
 *     quantity: 1
 *   }
 * }));
 *
 * If you just want to force refresh the mini-cart without adding a specific product, you can trigger the event
 * "cart:refresh" in a similar way (in that case, passing the quantity is not necessary):
 *
 * document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
 *   bubbles: true
 * }));
 */
$(document).ready(function() {

  $('body').on('click', '[data-siz-guide]',function(event){
    event.preventDefault();
    $('.modal[data-size]').attr('aria-hidden', 'false');
  });
  $('body').on('click', '[data-return-policy]',function(event){
    event.preventDefault();
    $('.modal[data-return-policy-modal]').attr('aria-hidden', 'false');
  });
  $('body').on('click', '.modal .close',function(event){
    event.preventDefault();
    $('.modal[data-size]').attr('aria-hidden', 'true');
    $('.modal[data-return-policy-modal]').attr('aria-hidden', 'true');
  });
  $(document).on('keydown', function(event) {
    if (event.key == "Escape") {
      if($('.modal[data-size]').attr('aria-hidden') == 'false'){
      	$('.modal[data-size]').attr('aria-hidden', 'true');
        $('.modal[data-return-policy-modal]').attr('aria-hidden', 'true');
      }
    }
  });
});